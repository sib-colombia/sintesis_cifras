# -*- coding: utf-8 -*-
"""
Created on Wed Jan 16 15:09:04 2019

"""

import pandas as pd
import numpy  as np
import shapely as shply
import geopandas as gpd
import fiona
from shapely.geometry import Point
import matplotlib.pyplot as plt
import warnings
import rtree
import csv
import json
import time
import requests
from pandas.io.json import json_normalize


# dwti=download toponim interpreted
dwti = pd.read_table('C:\\Cifras_Boyaca\\2019\\GBIFdata20191001\\ConsultaA_Text\\csv_interpretado_i.txt',encoding = "utf8", sep="\t")
# dwto=download toponim occurrece-DwCA
dwto = pd.read_table('C:\\Cifras_Boyaca\\2019\\GBIFdata20191001\\ConsultaA_Text\\occurrence_i.txt', encoding = "utf8")

# dwpi=download polygon interpreted
dwpi = pd.read_table('C:\\Cifras_Boyaca\\2019\\GBIFdata20191001\\ConsultaB_Polygon\\csv_interpretado_pol_i.txt',encoding = "utf8")
# dwpo=download polygon occurrece-DwCA
dwpo = pd.read_table('C:\\Cifras_Boyaca\\2019\\GBIFdata20191001\\ConsultaB_Polygon\\occurrence_pol_i.txt',encoding = "utf8")

#Refinamientos polígonos

#2.Crear un elemento nuevo 'Coordinates', con las coordenadas organizadas en un tupla
dwpi['Coordinates'] = list(zip(dwpi.decimalLongitude, dwpi.decimalLatitude))
dwpo['Coordinates'] = list(zip(dwpo.decimalLongitude, dwpo.decimalLatitude))

#3.Transformar la tupla en un elemento geométrico tipo punto
dwpi['Coordinates'] = dwpi['Coordinates'].apply(Point)
dwpo['Coordinates'] = dwpo['Coordinates'].apply(Point)


#4.Crear el geodataframe asignando el elemento que contiene la geometría
dwpi_g = gpd.GeoDataFrame(dwpi, geometry='Coordinates')
dwpo_g = gpd.GeoDataFrame(dwpo, geometry='Coordinates')

#5. Asignar el sistema de referencia al geodataframe
dwpi_g.crs = {'init' :'epsg:4326'}
dwpo_g.crs = {'init' :'epsg:4326'}

#6.Importar archivo shapefile (revisar la codificación de la capa y el sistema de referencia) preferiblemente que esté ya en WGS84, sino hay que realizar una conversión para que funcione el spatialJoin
BoyacaShp =gpd.read_file('C:\\Cifras_Boyaca\\Capas\\Boyaca_mpios.shp', encoding = "utf8")


# Unión espacial entre el geodataframe y el shapefile (primero el geodataframe, shapefile)
dwpi_gsj = gpd.sjoin(dwpi_g, BoyacaShp, how="inner", op= "intersects")
dwpo_gsj = gpd.sjoin(dwpo_g, BoyacaShp, how="inner", op="intersects")


#References checkList
#Cites Cleaned species matching
cites = pd.read_table('C:\\Cifras_Boyaca\\2019\\Listas\\CITES_speciesMatchCleaned_v20190124.txt')
# Amenazadas Cleaned species matching
threat = pd.read_table('C:\\Cifras_Boyaca\\2019\\Listas\\AmenazadasMADS_speciesMatchCleaned_v20190124.txt')
# Endemic Plantas Cleaned 
plantEM= pd.read_table('C:\\Cifras_Boyaca\\2019\\Listas\\PlantasEndemicas_v20190124.txt')
# Endemic  Fungi / Lichens Cleaned 
lichenEM= pd.read_table('C:\\Cifras_Boyaca\\2019\\Listas\\HongosEndemicas_v20190124.txt')
#Endemic Birds Cleaned 
birdsEM= pd.read_table('C:\\Cifras_Boyaca\\2019\\Listas\\AvesEndemicas_v20190124.txt')
#Endemic Birds Cleaned 
freshfEM= pd.read_table('C:\\Cifras_Boyaca\\2019\\Listas\\PecesDulceEndemicas_v20190124.txt')
#Endemic Birds Cleaned 
mammalEM= pd.read_table('C:\\Cifras_Boyaca\\2019\\Listas\\MamiferosEndemicas_v20190124.txt')
#Migratory birds
birdsMG= pd.read_table('C:\\Cifras_Boyaca\\2019\\Listas\\Datos_AvesMigratorias_v20190131.txt')
#Exóticas Fauna
animaliaExotica= pd.read_table('C:\\Cifras_Boyaca\\2019\\Listas\\Datos_FaunaExotica_v20190131.txt')
#Exóticas Flora
floraExotica= pd.read_table('C:\\Cifras_Boyaca\\2019\\Listas\\Datos_FloraExotica_v20190430.txt')


# concatenate (add rows) both interpreted downloads
dwi=pd.concat([dwti,dwpi_gsj], sort = False)
#concatenate (add rows) both occurrece-DwCA
dwo=pd.concat([dwto,dwpo_gsj], sort = False)
#concatenate (add rows) all endemic files
em=pd.concat([plantEM, lichenEM, birdsEM, freshfEM, mammalEM],sort = False)
#concatenate (add rows) all exotic files
ex=pd.concat([floraExotica,animaliaExotica])



# remove duplicates between interpreted downloads
dwiu=dwi.drop_duplicates('gbifID',inplace=False)
# remove duplicates between occurrece-DwCA downloads
dwou=dwo.drop_duplicates('gbifID',inplace=False)

#+ebird duplicates? or rather remove if yep!!!!!

#keep only selected (usefull) elements
dwiu_s=dwiu[["gbifID","datasetKey","publishingOrgKey","kingdom","phylum","class","order","family",
             "genus","species","taxonRank","scientificName","day","month","year",
             "decimalLatitude","decimalLongitude","locality"]]
dwou_s=dwou[["gbifID","waterBody","island","countryCode","stateProvince","county","municipality"]]


cites_s=cites[["species","appendixCITES"]]
threat_s=threat[["species","threatStatus"]]
em_s=em[["species","establishmentMeans"]]
em_s.columns = ["species","endemic"]
ex_s=ex[["species","establishmentMeans"]]
ex_s.columns = ["species","exotic"]
migratory_s=birdsMG[["species","migratory"]]



# working elements in a single file
dwm=pd.merge(dwiu_s,dwou_s)

#Homogenization of stateProvince toponim
dwm['stateProvince'] = dwm['stateProvince'].replace(["boyaca", "Boyaca","boyacá", "boy","Boy", "boyac", "Boyac", "boyac??", "Boyaca Department", 
   "Boyacá Department", "Boyaca Dept.", "boyac�","Boyac�", "CO-BOY", "Departamento de Boyaca", "Departamento de Boyacá", 
   "Dept. Boyaca", "Dpto. Boyacá", "Departamento de Boyac", "Dep. Boyacá", "Departamento Boyaca", "Boyacá Dept.", "Boyaca department", "BOYAC??", "Boyaca Dept.", "Boyaca dept."], 'Boyacá')


# concatenate (add rows) working elements with eBird 
#dwme=pd.concat([dwm,dweb_s], sort=False)
dwme=dwm

#Cross main dataframe with thematic information
dwme=dwme.merge(cites_s,on='species',how='left')
dwme=dwme.merge(threat_s,on='species',how='left')
dwme=dwme.merge(em_s,on='species',how='left')
dwme=dwme.merge(ex_s,on='species',how='left')
dwme=dwme.merge(migratory_s,on='species',how='left')


#Geographic spatial join. Get stateProvince and county according to cartograhpy 1:25000

#Fill empty coordinates with 0's
dwme[['decimalLatitude', 'decimalLongitude']] = dwme[['decimalLatitude','decimalLongitude']].fillna(value=0)
dwme['Coordinates'] = list(zip(dwme.decimalLongitude, dwme.decimalLatitude))
dwme['Coordinates'] = dwme['Coordinates'].apply(Point)
dwme_g = gpd.GeoDataFrame(dwme, geometry='Coordinates')
dwme_g.crs = {'init' :'epsg:4326'}
dwme_g = gpd.sjoin(dwme_g, BoyacaShp, how="left")
dwme_g = dwme_g.drop(['Area', 'index_right', 'gid'], axis=1)


#Export the result
dwme_g.to_csv('C:\\Cifras_Boyaca\\2019\\Resultados\\dwme_g.csv', sep=",", encoding = "utf8")
dwme_g.groupby(['species']).species.nunique().count()



#Call the GBIF API to get organization and dataset information

dwme_dk=dwme_g[["gbifID","datasetKey"]]
df=dwme_dk.drop_duplicates('datasetKey',inplace=False)

#DefinirVariable del llamado al API
def call_gbif(row):
    try:
        url = "http://api.gbif.org/v1/dataset/"+ str(row['datasetKey'])
        
        response = (requests.get(url).text)
        response_json = json.loads(response)
        time.sleep(0.25)
        return response_json
    
    except Exception as e:
        raise e

df['API_response'] = df.apply(call_gbif,axis=1)
norm_dk = json_normalize(df['API_response'])
dk = norm_dk[['key','license','doi','title','created','logoUrl']]
dk.rename(columns={'key': 'datasetKey'}, inplace=True)
dk.rename(columns={'title': 'datasetName'}, inplace=True)


#Call the GBIF API to get organization and dataset information

dwme_ok=dwme_g[["gbifID","publishingOrgKey"]]
dfo=dwme_ok.drop_duplicates('publishingOrgKey',inplace=False)

#DefinirVariable del llamado al API
def call_gbif(row):
    try:
        url = "http://api.gbif.org/v1/organization/"+ str(row['publishingOrgKey'])
        
        response = (requests.get(url).text)
        response_json = json.loads(response)
        time.sleep(0.25)
        return response_json
    
    except Exception as e:
        raise e

dfo['API_response'] = dfo.apply(call_gbif,axis=1)
norm_ok = json_normalize(dfo['API_response'])
ok = norm_ok[['key','title']]
ok.rename(columns={'key': 'publishingOrgKey'}, inplace=True)
ok.rename(columns={'title': 'organization'}, inplace=True)


#merge API call's to dataset
dwc_boy=pd.merge(dwme_g,dk,on='datasetKey')
dwc_boy=pd.merge(dwc_boy,ok,on='publishingOrgKey')

#Export the final dataset
dwc_boy.to_csv('C:\\Cifras_Boyaca\\2019\\Resultados\\dwc_boy20191001.txt', sep="\t", encoding = "utf8")


