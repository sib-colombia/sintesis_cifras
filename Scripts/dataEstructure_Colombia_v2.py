# -*- coding: utf-8 -*-
"""
Created on Wed Jan 31 15:09:04 2019

@authors: ricardo.ortiz 
"""

import pandas as pd
import numpy  as np
import shapely as shply
import geopandas as gpd
import fiona
from shapely.geometry import Point
import matplotlib.pyplot as plt
import warnings
import rtree
import csv
import json
import time
import requests
from pandas.io.json import json_normalize

#Loading the data previous processed whit the next elements: 
#dwi_s=dwi[["gbifID","datasetKey","publishingOrgKey","kingdom","phylum","collectionCode","institutionCode"
            # "class","order","family","genus","species","taxonRank","scientificName",
            # "day","month","year","decimalLatitude","decimalLongitude", "basisOfRecord", "elevation"]]
#dwo_s=dwo[["gbifID","repatriated","publishingCountry","countryCode","stateProvince","county","locality"]]


# dwti=load  interpreted
dwi = pd.read_table('C:\\Cifras_Colombia\\2018\\Descargas_GBIF\\dwi_s.txt', encoding = "utf8")
# dwti=load occurrece-DwCA
dwo = pd.read_table('C:\\Cifras_Colombia\\2018\\Descargas_GBIF\\dwo_s.txt', encoding = "utf8")



#merge dwi dwo
dwm=pd.merge(dwi,dwo,on='gbifID')
#remove eBird from datasets
dwm=dwm[dwm.datasetKey != '4fa7b334-ce0d-4e88-aaae-2e0c138d049e']
#Create a subset for INat and Xeno-Canto
xeno=dwm[dwm.datasetKey == 'b1047888-ae52-4179-9dd5-5448ea342a24']
inat=dwm[dwm.datasetKey == '50c9509d-22c7-4a22-a47d-8c48425ef4a7']
#remove repatriated from datasets
dwm=dwm[dwm.publishingCountry == 'CO']





# load eBird repatriated data
dwebird = pd.read_table('C:\\Cifras_Colombia\\2018\\Repatriados\\eBird\\ebd_repatriados_final.txt')
dwebird.rename(columns={'occurrenceID': 'gbifID'}, inplace=True)
dwebird_add = pd.read_table('C:\\Cifras_Colombia\\2018\\Repatriados\\eBird\\ebd_add.txt')
dweb=pd.merge(dwebird,dwebird_add,on='gbifID')
dweb_s=dweb[["gbifID","datasetKey","publishingOrgKey","kingdom","phylum","class","order","family","genus","species",
             "taxonRank","scientificName","day","month","year","decimalLatitude","decimalLongitude","countryCode","stateProvince","locality", "basisOfRecord",]]





# load GBIF repatriated data
dwgbif = pd.read_table('C:\\Cifras_Colombia\\2018\\Repatriados\\GBIF\\GBIF_2018.txt')
dwgbif.rename(columns={'occurrenceID': 'gbifID'}, inplace=True)
dwgbif_p = dwgbif[dwgbif.decimalLatitude.notnull()]
dwgbif_p = dwgbif_p[["gbifID","datasetID","institutionID","kingdom","phylum","class","order","family","genus","taxonRemarks","taxonRank","scientificName","day","month","year","decimalLatitude","decimalLongitude","stateProvince", "basisOfRecord","minimumElevationInMeters"]]
#Rename Columns: In taxonRemarks in eBird CO IPT´s publication is documented the species data from GBIF Backbone
dwgbif_p.rename(columns={'taxonRemarks': 'species'}, inplace=True)
dwgbif_p.rename(columns={'minimumElevationInMeteres': 'elevation'}, inplace=True)
dwgbif_p.rename(columns={'institutionID': 'publishingOrgKey'}, inplace=True)
dwgbif_p['publishingOrgKey'] = dwgbif_p.publishingOrgKey.replace({'https://www.gbif.org/publisher/':''}, regex=True)
dwgbif_p.rename(columns={'datasetID': 'datasetKey'}, inplace=True)
dwgbif_p['datasetKey']=dwgbif_p.datasetKey.replace({'https://www.gbif.org/dataset/':''}, regex=True)
#add element countryCode
dwgbif_p['countryCode'] = 'CO'



# concatenate (add rows) working elements with eBird 
dwme=pd.concat([dwm,dweb_s,dwgbif_p,xeno,inat], sort=False)


#Geographic spatial join. Get stateProvince and county according to cartograhpy 1:25000

#llenar coordenadas vacías con 0's
dwme[['decimalLatitude', 'decimalLongitude']] = dwme[['decimalLatitude','decimalLongitude']].fillna(value=0)
dwme['Coordinates'] = list(zip(dwme.decimalLongitude, dwme.decimalLatitude))
dwme['Coordinates'] = dwme['Coordinates'].apply(Point)
dwme_g = gpd.GeoDataFrame(dwme, geometry='Coordinates')
dwme_g.crs = {'init' :'epsg:4326'}
#Importar archivo shapefile (revisar la codificación de la capa y el sistema de referencia) preferiblemente que esté ya en WGS84, sino hay que realizar una conversión para que funcione el spatialJoin
colombia25 =gpd.read_file('C:\\Cifras_Colombia\\Capas\\Municipal_Colombia_25K.shp', encoding = "utf8")
dwme_g = gpd.sjoin(dwme_g, colombia25, how="left")
dwme_g = dwme_g.drop(['Area', 'index_right'], axis=1)



#References checkList
#Cites Cleaned species matching
cites = pd.read_table('C:\\Cifras_Boyaca\\2018\\Listas\\CITES_speciesMatchCleaned_v20190124.txt')
# Amenazadas Cleaned species matching
threat = pd.read_table('C:\\Cifras_Boyaca\\2018\\Listas\\AmenazadasMADS_speciesMatchCleaned_v20190124.txt')
# Endemic Plantas Cleaned 
plantEM= pd.read_table('C:\\Cifras_Boyaca\\2018\\Listas\\PlantasEndemicas_v20190124.txt')
# Endemic  Fungi / Lichens Cleaned 
lichenEM= pd.read_table('C:\\Cifras_Boyaca\\2018\\Listas\\HongosEndemicas_v20190124.txt')
#Endemic Birds Cleaned 
birdsEM= pd.read_table('C:\\Cifras_Boyaca\\2018\\Listas\\AvesEndemicas_v20190124.txt')
#Endemic Birds Cleaned 
freshfEM= pd.read_table('C:\\Cifras_Boyaca\\2018\\Listas\\PecesDulceEndemicas_v20190124.txt')
#Endemic Birds Cleaned 
mammalEM= pd.read_table('C:\\Cifras_Boyaca\\2018\\Listas\\MamiferosEndemicas_v20190124.txt')
#Migratory birds
birdsMG= pd.read_table('C:\\Cifras_Boyaca\\2018\\Listas\\Datos_AvesMigratorias_v20190131.txt')
#Exotic fauna
faunaEX= pd.read_table('C:\\Cifras_Boyaca\\2018\\Listas\\Datos_FaunaExotica_v20190131.txt')


cites_s=cites[["species","appendixCITES"]]
threat_s=threat[["species","threatStatus"]]
#concatenate (add rows) all endemic files
em=pd.concat([plantEM, lichenEM, birdsEM, freshfEM, mammalEM],sort = False)
em_s=em[["species","establishmentMeans"]]
mi_s=birdsMG[["species","migratory"]]
ex_s=faunaEX[["species","exotic"]]


#Cross main dataframe with thematic information
dwme_t=dwme_g.merge(cites_s,on='species',how='left')
dwme_t=dwme_t.merge(threat_s,on='species',how='left')
dwme_t=dwme_t.merge(em_s,on='species',how='left')
dwme_t=dwme_t.merge(mi_s,on='species',how='left')
dwme_t=dwme_t.merge(ex_s,on='species',how='left')


#Call the GBIF API to get organization and dataset information

dwme_dk=dwme_t[["gbifID","datasetKey"]]
df=dwme_dk.drop_duplicates('datasetKey',inplace=False)

#DefinirVariable del llamado al API
def call_gbif(row):
    try:
        url = "http://api.gbif.org/v1/dataset/"+ str(row['datasetKey'])
        
        response = (requests.get(url).text)
        response_json = json.loads(response)
        time.sleep(0.25)
        return response_json
    
    except Exception as e:
        raise e

df['API_response'] = df.apply(call_gbif,axis=1)
norm_dk = json_normalize(df['API_response'])
dk = norm_dk[['key','license','doi','title','created','logoUrl']]
dk.rename(columns={'key': 'datasetKey'}, inplace=True)
dk.rename(columns={'title': 'datasetName'}, inplace=True)


#Call the GBIF API to get organization and dataset information

dwme_ok=dwme_t[["gbifID","publishingOrgKey"]]
dfo=dwme_ok.drop_duplicates('publishingOrgKey',inplace=False)

#DefinirVariable del llamado al API
def call_gbif(row):
    try:
        url = "http://api.gbif.org/v1/organization/"+ str(row['publishingOrgKey'])
        
        response = (requests.get(url).text)
        response_json = json.loads(response)
        time.sleep(0.25)
        return response_json
    
    except Exception as e:
        raise e

dfo['API_response'] = dfo.apply(call_gbif,axis=1)
norm_ok = json_normalize(dfo['API_response'])
ok = norm_ok[['key','title']]
ok.rename(columns={'key': 'publishingOrgKey'}, inplace=True)
ok.rename(columns={'title': 'organization'}, inplace=True)


#merge API call's to dataset
dwc_co=pd.merge(dwme_t,dk,on='datasetKey')
dwc_co=pd.merge(dwc_co,ok,on='publishingOrgKey')



#Export the final dataset
dwc_co.to_csv('C:\\Cifras_Colombia\\2018\\dwc_co.txt', sep="\t", encoding = "utf8")
dwgbif_p.to_csv('C:\\Cifras_Colombia\\2018\\Repatriados\GBIF\\gbif_repatriated_not_doubtfull.txt', sep="\t", encoding = "utf8")


